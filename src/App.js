import React, { useState } from 'react';

import { BotInput, BotOutput, BotWrapper } from './components';

import './App.css';

function App() {
  const [initialSend, setInitialSend] = useState(false);
  const [sending, setSending] = useState(false);
  const [content, setContent] = useState(null);

  const showContent = async (content) => {
    setTimeout(() => {
      setContent(content);
      setSending(false);
    }, 500);
  }

  const send = async (inputValue) => {
    if (!initialSend) {
      setInitialSend(true);
    }

    setSending(true);
    setTimeout(async () => {
      setContent(null);
      const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
      try {
        const response = await fetch(
          'https://api.klimabot.de/v1/query',
          {
            headers,
            method: "POST",
            body: JSON.stringify({question: inputValue})
          }
        )
        const result = await response.json()
        if (result['intent'] === 'none') {
          await showContent("Leider weiß ich auf diese Frage keine Antwort.");
        } else {
          if (result.answer) {
            await showContent(result.answer);
          } else {
            const json = JSON.stringify(result, null, 4)
            await showContent(`Leider habe ich auf diese Frage noch keine Antwort parat,<br/>aber ich arbeite fleißig daran.<br/> Hier ist, was ich erkannt habe: <br/><br/><pre class="jsonContent"><code>${json}</code></pre>`);
          }
        }
      } catch (e) {
        console.error(e);
        await showContent("Es tut mir leid, aber leider ist mir gerade ein Fehler unterlaufen. Bitte versuchen Sie es später erneut.");
      }
    }, 300);
  }

  return (
    <div className="app">
      <div className="content">
        <BotWrapper>
          <BotInput sending={sending} initialSend={initialSend} send={send} />
          <BotOutput sending={sending} initialSend={initialSend} content={content}/>
        </BotWrapper>
      </div>
    </div>
  );
}

export default App;
