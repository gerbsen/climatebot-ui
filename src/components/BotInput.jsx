import React, { useState } from 'react';

import { Button,  TextField, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: theme.spacing(0.8),
    marginRight: theme.spacing(0.5),
    width: 800,
  },
  button: {
    marginTop: '8px',
  }
}));

export default function BotInput({ sending, setSending, initialSend, setInitialSend, setContent, send }) {
  const [inputValue, setInputValue] = useState();
  const cls = useStyles();

  const onEnter = (e) => {
    if (e.key === 'Enter' && inputValue) {
        send(inputValue);
    };
  };

  const submit = () => {send(inputValue)}

  return (
    <>
      <TextField
        className={cls.textField}
        id="bot-input"
        label="Was möchtest du wissen? Gib eine Frage ein wie: 'Wie viel CO2 hat Deutschland produziert?'"
        disabled={sending}
        onKeyDown={onEnter}
        onChange={(e) => {
          setInputValue(e.target.value)
        }}
      />
      <Button variant="contained" className={cls.button} disabled={sending || !inputValue} onClick={submit}>
        Senden
      </Button>
    </>
  )
}
