import React, { useEffect } from 'react';

import { useSpring, animated } from 'react-spring'
import { makeStyles, CircularProgress } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  output: {
    height: 0,
    willChange: 'height',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  outputWrapperInner: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  contentWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
  },
  dangerousContentDiv: {
    width: '100%',
    height: '100%',
    fontSize: '70%',
    textAlign: 'justify',
  }
}));


export default function BotOutput({ sending, content, initialSend}) {
  const cls = useStyles();

  const [openProps, setOpenProps] = useSpring(() => ({height: 0}))
  const [spinnerProps, setSpinnerProps] = useSpring(() => ({opacity: 1}))
  const [contentProps, setContentProps] = useSpring(() => ({opacity: 0}))

  useEffect(() => {
    if (initialSend) {
      setOpenProps({height: 400});
    }
  }, [initialSend, setOpenProps]);

  useEffect(() => {
    if (sending) {
      setSpinnerProps({opacity: 1});
      setContentProps({opacity: 0});
    } else {
      setSpinnerProps({opacity: 0})
      setContentProps({opacity: 1})
    }
  }, [sending, setSpinnerProps, setContentProps]);

  return (
    <animated.div className={cls.output} style={openProps}>
      {initialSend &&
        <div className={cls.outputWrapperInner}>
          <animated.div className={cls.contentWrapper} style={spinnerProps}>
            <CircularProgress />
          </animated.div>
          <animated.div className={cls.contentWrapper} style={contentProps}>
            <div className={cls.dangerousContentDiv} dangerouslySetInnerHTML={{__html: content ? `<br/><br/> ${content}` : ''}}></div>
          </animated.div>
        </div>
      }
    </animated.div>
  );
}
