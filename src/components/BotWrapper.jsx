import React from 'react';

import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  botWrapper: {
    backgroundColor: '#FFFFFF',
    padding: '0.7em',
    borderRadius: '4px',
  }
}));

export default function BotWrapper({ children }) {
  const cls = useStyles();

  return (
    <div className={cls.botWrapper}>
      {children}
    </div>
  )
}
